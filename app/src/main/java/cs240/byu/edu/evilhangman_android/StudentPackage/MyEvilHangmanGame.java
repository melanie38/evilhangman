package cs240.byu.edu.evilhangman_android.StudentPackage;

import android.support.annotation.NonNull;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by melanie on 07/05/2016.
 */
public class MyEvilHangmanGame implements StudentEvilHangmanGameController {
    private int numberOfGuessesToStart;
    private int numberOfGuessesLeft;
    private int wordLength;
    private char guess;
    private char[] currentWord;
    private String keyToReturn;
    private HashSet<Character> usedLetters;
    private HashSet<String> myDictionary;
    private HashMap<String, HashSet<String>> setOfWord;


    public MyEvilHangmanGame() {
        numberOfGuessesToStart = 0;
        numberOfGuessesLeft = 0;
        wordLength = 0;
        keyToReturn = new String();
        //currentWord = new char[wordLength];
        usedLetters = new HashSet<>();
        myDictionary = new HashSet<>();
        setOfWord = new HashMap<>();

    }

    public boolean wordGuessed() {
        if (getCurrentWord().contains("-")) {
            return false;
        }
        return true;
    }

    @Override
    public GAME_STATUS getGameStatus() {
        if (numberOfGuessesLeft == 0 && !wordGuessed()) {
            return GAME_STATUS.PLAYER_LOST;
        }
        if (wordGuessed()) {
            return GAME_STATUS.PLAYER_WON;
        }
        return GAME_STATUS.NORMAL;
    }

    @Override
    public int getNumberOfGuessesLeft() {
        return numberOfGuessesLeft;
    }

    @Override
    public String getCurrentWord() {
        if (!keyToReturn.isEmpty()) {
            for (int i = 0; i < wordLength; i++) {
                if (keyToReturn.charAt(i) == '1') {
                    currentWord[i] = guess;
                }
            }
        }
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < wordLength; i++) {
            temp.append(currentWord[i]);
        }
        return temp.toString();
    }

    @Override
    public Set<Character> getUsedLetters() { return usedLetters; }

    @Override
    public void setNumberOfGuesses(int numberOfGuessesToStart) {
        this.numberOfGuessesToStart = numberOfGuessesToStart;
    }

    @Override
    public void startGame(InputStreamReader dictionary, int wordLength) {
        // Initializations
        myDictionary.clear();
        usedLetters.clear();
        this.wordLength = wordLength;
        currentWord = new char[wordLength];
        numberOfGuessesLeft = numberOfGuessesToStart;

        for (int i = 0; i < wordLength; i++) {
            currentWord[i] = '-';
        }

        // create my dictionary containing words = wordLength
        BufferedReader line = new BufferedReader(dictionary);
        try {
            String word = line.readLine();
            while (word != null) {
                if (word.length() == wordLength) {
                    word.toLowerCase();
                    myDictionary.add(word);
                }
                word = line.readLine();
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set<String> makeGuess(char guess) throws GuessAlreadyMadeException {
        this.guess = guess;
        setOfWord.clear();

        // update the set of characters used
        if (usedLetters.contains(guess)) {
            throw new GuessAlreadyMadeException();
        }
        usedLetters.add(guess);
        numberOfGuessesLeft--;  // update the number of guesses left

        myDictionary = setOfWord.get(sortDictionary(guess));
        return myDictionary;
    }
    public String sortDictionary(char guess) {
        populateMap(guess);
        return compareSets();

    }
    public void populateMap(char guess) {
        for (String word : myDictionary) {
            // create a key based on the position of the letter(s)
            StringBuilder key = new StringBuilder();
            // associate the key with the word
            for (int i = 0; i < wordLength; i++) {
                if (word.charAt(i) == guess) {
                    key.append('1');
                }
                else {
                    key.append('0');
                }
            }
            if (setOfWord.containsKey(key.toString())) {
                setOfWord.get(key.toString()).add(word);
            }
            else {
                HashSet<String> tempSet = new HashSet<>();
                setOfWord.put(key.toString(), tempSet);
                setOfWord.get(key.toString()).add(word);
            }
        }
    }
    public String compareSets() {
        int ref = 0;

        for (String key : setOfWord.keySet()) {
            int size = setOfWord.get(key).size();
            if (size > ref) {
                ref = size;
                keyToReturn = key;
            }
            else if (size == ref) {
                // compare the number of letter guessed in each set
                int countKey = 0;
                int countKeyToReturn = 0;
                for (int i = 0; i < key.length(); i++) {
                    if (key.charAt(i) == '1') { countKey++; }
                    if (keyToReturn.charAt(i) == '1') { countKeyToReturn++; }
                }
                if (countKey < countKeyToReturn) { keyToReturn = key; }
                else if( countKey == countKeyToReturn) {
                    // find which set has the rightmost guessed letter
                    for (int i = 0; i < key.length(); i++) {
                        if (key.charAt(i) < keyToReturn.charAt(i)) {
                            keyToReturn = key;
                            break;
                        }
                        else if (key.charAt(i) > keyToReturn.charAt(i)) {
                            break;
                        }
                    }
                }
            }
        }
        return keyToReturn;
    }
}
